.. ATL Data API Schemas documentation master file, created by
   sphinx-quickstart on Wed Nov 28 14:24:34 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ATL Data API Schemas's documentation!
================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   classes


Key Classes:

* class `SubjectReport <classes.html#atollogy.data.schemas.reports.subject.subject_report.SubjectReport>`_
* class `SubjectStatusEvent <classes.html#atollogy.data.schemas.reports.subject.subject_report.SubjectStatusEvent>`_
* class `AssetStatusEvent <classes.html#atollogy.data.schemas.reports.subject.subject_report.AssetStatusEvent>`_

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
