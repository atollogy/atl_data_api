
import json
import os
from typing import List

from pydantic import ValidationError

from atollogy.data.schemas.config.hue_vector_config \
    import HueVectorConfig, HueSubjectDetails

from atollogy.data.inheritance_merge.merge_funcs import inheritance_merge
from atollogy.data.schemas.config.mv_routing_config import MVRouting

FILE_BASE_DIR = "./sample_data/atollogy/data/inheritance_merge/"


def load_json(filename):
    with open(os.path.join(FILE_BASE_DIR, filename)) as f:
        return json.load(f)


def _get_json_dict_list_01(file_list: List) -> List[str]:
    file_json_list = []
    for file in file_list:
        json_dict = load_json(file)
        file_json_list.append(json_dict)
    return file_json_list


def test_pos_01():
    json_list = _get_json_dict_list_01(
        ["mv_routing_input_01.json",
         "mv_routing_input_02.json",
         "mv_routing_input_03.json"])
    merged_config_dict, merged_config_obj = inheritance_merge(
        json_list, data_class=MVRouting)
    assert merged_config_dict is not None
    assert merged_config_obj is not None

    merged_config_obj: MVRouting = merged_config_obj

    assert merged_config_obj.steps == ["operator", "status_hue"]
    assert set(merged_config_obj.stepCfgs.keys()) == set(["operator", "status_hue"])

    hue_cfg = merged_config_obj.get_step_config_obj("status_hue")
    assert hue_cfg is not None
    assert isinstance(hue_cfg, HueVectorConfig)

    hue_cfg: HueVectorConfig = hue_cfg
    hue_details: HueSubjectDetails = hue_cfg.subjects["light_hue"]
    assert hue_details is not None

    assert (160, 32, 240) == hue_details.rgb_vector_labels["purple"]


def test_neg_01():
    json_list = _get_json_dict_list_01(
        ["mv_routing_input_01.json",
         "mv_routing_input_02.json",
         "mv_routing_input_03.json"])

    wrong_step_name = "status_huex"
    json_list[1]["steps"] = [wrong_step_name]

    value_error = None
    try:
        inheritance_merge(json_list, data_class=MVRouting)
    except ValueError as e:
        value_error = e

    assert value_error is not None
    assert wrong_step_name in str(value_error)


def test_neg_02():
    json_list = _get_json_dict_list_01(
        ["mv_routing_input_01.json",
         "mv_routing_input_02.json",
         "mv_routing_input_03.json"])

    subject_details = json_list[1]["stepCfgs"]["status_hue"]["subjects"]["light_hue"]
    subject_details["rgb_vector_labels"]["yellow"] = [167, 146, 102, 444]
    # incorrect usage of 4 integers, instead of 3 integers (R, G, B)

    print("ayiu: json_list[1]={}".format(json_list[1]))

    value_error = None
    try:
        inheritance_merge(json_list, data_class=MVRouting)
    except ValueError as e:
        value_error = e

    assert value_error is not None
    assert "yellow" in str(value_error)

