
import json
import os

from atollogy.data.schemas.reports.subject.subject_report \
    import SubjectStatusEvent, AssetStatusEvent

FILE_BASE_DIR = "./sample_data/atollogy/data/schemas/reports/subject/"


def load_json(filename):
    with open(os.path.join(FILE_BASE_DIR, filename)) as f:
        return json.load(f)


def test_subject_report_airport_01_json_positive():
    json_dict = load_json("sample_subject_event_airport_01.json")
    SubjectStatusEvent(**json_dict)

    json_dict = load_json("sample_subject_event_airport_02.json")
    SubjectStatusEvent(**json_dict)

    json_dict = load_json("sample_subject_event_airport_03.json")
    SubjectStatusEvent(**json_dict)

    json_dict = load_json("sample_subject_event_airport_04.json")
    SubjectStatusEvent(**json_dict)

    json_dict = load_json("sample_subject_event_airport_05.json")
    SubjectStatusEvent(**json_dict)


def test_asset_event_01():
    json_dict = load_json("sample_asset_event_yard_01.json")
    asset_event = AssetStatusEvent(**json_dict)
    assert "Scale 2" == asset_event.subject_name
    assert "tractor" == asset_event.asset_id.asset_type
    assert "lbs" == asset_event.asset_properties.weight.weight_unit
