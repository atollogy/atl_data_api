
import json
import os
import datetime

from pydantic import ValidationError

from atollogy.data.schemas.reports.subject.subject_report import SubjectReport

FILE_BASE_DIR = "./sample_data/atollogy/data/schemas/reports/subject/"


def load_json(filename):
    with open(os.path.join(FILE_BASE_DIR, filename)) as f:
        return json.load(f)


def test_subject_report_airport_01_json_positive():
    json_dict = load_json("sample_subject_report_airport_01.json")
    subject_report = SubjectReport(**json_dict)

    agg_stat = subject_report.aggregate_stats.get("Average Cycle Time")
    assert agg_stat is not None
    assert datetime.timedelta(days=1) == agg_stat.stat_comparison.prev_period_iso8601_duration

    composite_timeline = subject_report.timelines.get("composite")
    assert composite_timeline is not None

    assert False == composite_timeline.timeline_periods[1].data_unavailable
    # expecting default "False" value takes effect

    assert True == composite_timeline.timeline_periods[2].data_unavailable


def test_subject_report_airport_01_json_negative_end_time_greater_start_time():
    json_dict = load_json("sample_subject_report_airport_01.json")
    json_dict["timelines"]["composite"]["timeline_periods"][0]["start_time"] = {
        "iso8601_datetime": "2018-11-02T00:00:00Z",
        "epoch_second": 1541116800
    }
    ve = None
    try:
        SubjectReport(**json_dict)
    except ValueError as e:
        ve = e
    assert ve is not None


def test_subject_report_airport_01_json_negative_end_time_missing_in_middle():
    json_dict = load_json("sample_subject_report_airport_01.json")
    json_dict["timelines"]["composite"]["timeline_periods"][1]["end_time"] = None
    ve = None
    try:
        SubjectReport(**json_dict)
    except ValueError as e:
        ve = e
    assert ve is not None


def test_subject_report_airport_01_json_negative_end_time_missing_in_middle():
    json_dict = load_json("sample_subject_report_airport_01.json")
    json_dict["timelines"]["composite"]["timeline_periods"][1]["end_time"] = {
        "iso8601_datetime": "2018-11-01T01:35:45Z",
        "epoch_second": 1541036145
    }
    ve = None
    try:
        SubjectReport(**json_dict)
    except ValueError as e:
        ve = e
    assert ve is not None


def test_subject_report_airport_01_json_ingore_extra_disabled():

    class StricterSubjectReport(SubjectReport):
        class Config:
            ignore_extra: bool = False
            # this "ignore_extra" option applies to the top level data object only
            # (i.e. "SubjectReport").
            # Other nested objects inside "SubjectReport" is not affected
            # by this deserialization option.

    json_dict = load_json("sample_subject_report_airport_01.json")

    subject_report = StricterSubjectReport(**json_dict)
    assert subject_report is not None

    # adding an extra dummy field:
    json_dict["dummy_extra"] = "dummy_value"

    ve = None
    try:
        StricterSubjectReport(**json_dict)
    except ValidationError as e:
        ve = e
    assert ve is not None
