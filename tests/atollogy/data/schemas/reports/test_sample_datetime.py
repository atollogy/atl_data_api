
import json
import os

import pytz

from atollogy.data.schemas.reports.datetime import DateTimeIso8601Result, LocalTimezoneEnum

FILE_BASE_DIR = "./sample_data/atollogy/data/schemas/reports/"


def load_json(filename):
    with open(os.path.join(FILE_BASE_DIR, filename)) as f:
        return json.load(f)


def test_datetime_json_01_positive():
    json_dict = load_json("sample_datetime_01.json")
    datetime_result = DateTimeIso8601Result(**json_dict)

    assert datetime_result.iso8601_datetime.utcoffset() is None
    assert LocalTimezoneEnum.user == datetime_result.local_timezone

    # assume local timezone of user is "PST" (i.e. "America/Los_Angeles") under this test
    user_timezone = pytz.timezone("America/Los_Angeles")
    # applying timezone to datetime object
    localized_datetime = user_timezone.localize(datetime_result.iso8601_datetime)

    assert datetime_result.iso8601_datetime.utcoffset() is None
    assert localized_datetime.utcoffset() is not None
    assert datetime_result.epoch_second == localized_datetime.timestamp()


def test_datetime_json_02_positive():
    json_dict = load_json("sample_datetime_02.json")
    datetime_result = DateTimeIso8601Result(**json_dict)
    assert datetime_result.iso8601_datetime.utcoffset() is not None
    assert datetime_result.local_timezone is None
    assert datetime_result.epoch_second == datetime_result.iso8601_datetime.timestamp()


def test_datetime_json_01_negative_wrong_local_timezone():
    json_dict = load_json("sample_datetime_01.json")
    json_dict["local_timezone"] = "typo"
    ve = None
    try:
        DateTimeIso8601Result(**json_dict)
    except ValueError as e:
        ve = e
    assert ve is not None


def test_datetime_json_01_negative_wrong_iso_8601_datetime():
    json_dict = load_json("sample_datetime_01.json")
    json_dict["iso8601_datetime"] = "2018-11-01T30:23:45"
    ve = None
    try:
        DateTimeIso8601Result(**json_dict)
    except ValueError as e:
        ve = e
    assert ve is not None


def test_datetime_json_01_negative_missing_local_timezone():
    json_dict = load_json("sample_datetime_01.json")
    json_dict.pop("local_timezone", None)
    ve = None
    try:
        DateTimeIso8601Result(**json_dict)
    except ValueError as e:
        ve = e
    assert ve is not None


def test_datetime_json_02_negative_extra_local_timezone():
    json_dict = load_json("sample_datetime_02.json")
    json_dict["local_timezone"] = "user"
    ve = None
    try:
        DateTimeIso8601Result(**json_dict)
    except ValueError as e:
        ve = e
    assert ve is not None


def test_datetime_json_02_negative_inconsistent_epoch():
    json_dict = load_json("sample_datetime_02.json")
    json_dict["epoch_second"] = 1541060626
    ve = None
    try:
        DateTimeIso8601Result(**json_dict)
    except ValueError as e:
        ve = e
    assert ve is not None
