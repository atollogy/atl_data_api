
import json
import os

from atollogy.data.schemas.mv_results.mv_inference_reqs \
    import MvInferenceRequestBase

from atollogy.data.schemas.mv_results.mv_image_results \
    import ObjectDetectionResult, ImageClassificationResult, VehicleClassificationIDResult

FILE_BASE_DIR = "./sample_data/atollogy/data/schemas/mv_results"


def load_json(filename):
    with open(os.path.join(FILE_BASE_DIR, filename)) as f:
        return json.load(f)


def test_object_detection_01_json_positive():
    json_dict = load_json("mv_human_object_detection_annotation_result_01.json")
    ObjectDetectionResult(**json_dict)

def test_img_classifcation_01_json_positive():
    json_dict = load_json("mv_human_image_classification_annotation_result_01.json")
    ImageClassificationResult(**json_dict)

def test_vehicle_classifcation_01_json_positive():
    json_dict = load_json("mv_human_vehicle_classification_annotation_result_01.json")
    VehicleClassificationIDResult(**json_dict)

def test_people_detection_req_01_json_positive():
    json_dict = load_json("mv_people_detection_req_sample.json")
    MvInferenceRequestBase(**json_dict)
