import os
import json
from typing import Optional

from atollogy.data.schemas.mv_results.mv_image_results import QCObjectDetectionResult


FILE_BASE_DIR = "./sample_data/atollogy/data/schemas/mv_results"


def load_json(filename):
    with open(os.path.join(FILE_BASE_DIR, filename)) as f:
        return json.load(f)

def test_qc_obj_detection_empty_result() -> None:
    QCObjectDetectionResult(qc_object_list=[])


def test_qc_obj_detection_result() -> None:
    json_dict = load_json("qc_obj_detection_result_01.json")
    result = QCObjectDetectionResult(**json_dict)
    assert result.qc_object_list[0].object_qc_type == "not_sure"


def test_qc_obj_detection_invalid_result() -> None:
    invalid_file_list = [
        "qc_obj_detection_invalid_result_01.json",
        "qc_obj_detection_invalid_result_02.json",
        "qc_obj_detection_invalid_result_03.json",
        "qc_obj_detection_invalid_result_04.json",
        "qc_obj_detection_invalid_result_05.json",
        "qc_obj_detection_invalid_result_06.json",
        "qc_obj_detection_invalid_result_07.json",
    ]
    for file_name in invalid_file_list:
        value_err: Optional[ValueError] = None
        json_dict = load_json(file_name)
        try:
            print("verifying json data from: {}".format(file_name))
            QCObjectDetectionResult(**json_dict)
        except ValueError as err:
            value_err = err
        assert value_err is not None

