

import json
from typing import List

from atollogy.data.schemas.mv_results.apply_qc_result import apply_detected_obj_qc_result

from atollogy.data.schemas.mv_results.mv_image_results import QCObjectDetectionResult, ObjectDetectionResult, \
    DetectedObjectGroup


def test_apply_obj_qc_01():
    qc_obj_result_str = """
{
  "qc_object_list": [
    {
       "object_qc_type": "not_sure",
       "target_class_text": "person",
       "target_obj_idx": 0
    },
    {
       "object_qc_type": "correct",
       "target_class_text": "person",
       "target_obj_idx": 1
    },
    {
       "object_qc_type": "chg_bbox",
       "target_class_text": "person",
       "target_obj_idx": 2,
       "new_bbox": {
         "xmin": 12,
         "xmax": 111,
         "ymin": 22,
         "ymax": 222
       }
    },
    {
       "object_qc_type": "chg_class",
       "target_class_text": "person",
       "target_obj_idx": 3,
       "new_class": "garbage"
    },    
    {
       "object_qc_type": "chg_bbox_class",
       "target_class_text": "person",
       "target_obj_idx": 4,
       "new_class": "garbage",
       "new_bbox": {
         "xmin": 14,
         "xmax": 111,
         "ymin": 22,
         "ymax": 222
       }
    },
    {
       "object_qc_type": "false_negative",
       "new_class": "person",
       "new_bbox": {
         "xmin": 15,
         "xmax": 111,
         "ymin": 22,
         "ymax": 222
       }
    }, 
    {
       "object_qc_type": "false_positive",
       "target_class_text": "person",
       "target_obj_idx": 5
    }
  ]
}
    """
    qc_obj_result_obj = QCObjectDetectionResult(**json.loads(qc_obj_result_str))

    orig_obj_result_str = """
{
    "s3_url": "s3://dummy-bucket/dummy.jpg" , 
    "height": 1234, 
    "width": 789, 
    "result_gen_time": {
        "iso8601_datetime": "2019-10-05T14:00:53Z"
    },
    "result_source_type": "model_result", 
    "object_group_list": [ 
        {
            "detected_object_class": { "text": "person" }, 
            "detected_object_list": [ 
                { 
                    "bbox": {"xmin": 0, "ymin": 2, "xmax": 103, "ymax": 104},
                    "confidence_score": 0.8
                },
                { 
                    "bbox": {"xmin": 1, "ymin": 2, "xmax": 103, "ymax": 104},
                    "confidence_score": 0.8
                },
                { 
                    "bbox": {"xmin": 2, "ymin": 2, "xmax": 103, "ymax": 104},
                    "confidence_score": 0.8
                },
                { 
                    "bbox": {"xmin": 3, "ymin": 2, "xmax": 103, "ymax": 104},
                    "confidence_score": 0.8
                },
                { 
                    "bbox": {"xmin": 4, "ymin": 2, "xmax": 103, "ymax": 104},
                    "confidence_score": 0.8
                },
                { 
                    "bbox": {"xmin": 5, "ymin": 2, "xmax": 103, "ymax": 104},
                    "confidence_score": 0.8
                },
                { 
                    "bbox": {"xmin": 6, "ymin": 2, "xmax": 103, "ymax": 104},
                    "confidence_score": 0.8
                }
            ]
        },
        {
            "detected_object_class": { "text": "garbage" }, 
            "detected_object_list": [ 
                { 
                    "bbox": {"xmin": 0, "ymin": 202, "xmax": 103, "ymax": 304},
                    "confidence_score": 0.8
                }
            ]
        },
        {
            "detected_object_class": { "text": "truck" }, 
            "detected_object_list": [ 
                { 
                    "bbox": {"xmin": 0, "ymin": 302, "xmax": 103, "ymax": 404},
                    "confidence_score": 0.8
                }
            ]
        }
    ]
}    
    """
    orig_obj_result_obj = ObjectDetectionResult(**json.loads(orig_obj_result_str))
    new_list = apply_detected_obj_qc_result(
        qc_obj_result_obj.qc_object_list, orig_obj_result_obj.object_group_list)

    golden_apply_result_str = """
[
    {
        "detected_object_class": {
            "text": "person"
        },
        "detected_object_list": [
            {
                "bbox": {
                    "xmin": 0,
                    "xmax": 103,
                    "ymin": 2,
                    "ymax": 104
                },
                "confidence_score": 0.8
            },
            {
                "bbox": {
                    "xmin": 1,
                    "xmax": 103,
                    "ymin": 2,
                    "ymax": 104
                },
                "confidence_score": 0.8
            },
            {
                "bbox": {
                    "xmin": 12,
                    "xmax": 111,
                    "ymin": 22,
                    "ymax": 222
                },
                "confidence_score": null
            },
            {
                "bbox": {
                    "xmin": 6,
                    "xmax": 103,
                    "ymin": 2,
                    "ymax": 104
                },
                "confidence_score": 0.8
            },
            {
                "bbox": {
                    "xmin": 15,
                    "xmax": 111,
                    "ymin": 22,
                    "ymax": 222
                },
                "confidence_score": null
            }
        ]
    }
    ,
    {
        "detected_object_class": {
            "text": "garbage"
        },
        "detected_object_list": [
            {
                "bbox": {
                    "xmin": 0,
                    "xmax": 103,
                    "ymin": 202,
                    "ymax": 304
                },
                "confidence_score": 0.8
            },
            {
                "bbox": {
                    "xmin": 3,
                    "xmax": 103,
                    "ymin": 2,
                    "ymax": 104
                },
                "confidence_score": null
            },
            {
                "bbox": {
                    "xmin": 14,
                    "xmax": 111,
                    "ymin": 22,
                    "ymax": 222
                },
                "confidence_score": null
            }
        ]
    }
    ,
    {
        "detected_object_class": {
            "text": "truck"
        },
        "detected_object_list": [
            {
                "bbox": {
                    "xmin": 0,
                    "xmax": 103,
                    "ymin": 302,
                    "ymax": 404
                },
                "confidence_score": 0.8
            }
        ]
    }
]
    """


    golden_apply_result_json_list = json.loads(golden_apply_result_str)
    golden_result_obj_list: List[DetectedObjectGroup] = [
        DetectedObjectGroup(**dict_entry)
        for dict_entry in golden_apply_result_json_list
    ]

    """
    for entry in new_list:
        print("--------------------")
        print(entry.json(indent=4))
        print("--------------------")
    """
    assert new_list == golden_result_obj_list
