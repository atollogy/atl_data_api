
import json
import os

from atollogy.data.schemas.camera_configuration.atl_cam_config import CameraConfiguration

FILE_BASE_DIR = "./sample_data/atollogy/data/schemas/camera_configuration/"


def load_json(filename):
    with open(os.path.join(FILE_BASE_DIR, filename)) as f:
        return json.load(f)



def test_cam_settings_positive_01():
    json_dict = load_json("sample_atl_canonical_camera_configuration.json")
    camera_configuration = CameraConfiguration(**json_dict)
    assert camera_configuration.camera_channel_title == "db8cam02_atl_prd"

def test_cam_settings_positive_02():
    json_dict = load_json("sample_atl_canonical_camera_configuration.json")
    camera_configuration = CameraConfiguration(**json_dict)
    assert camera_configuration.ir_settings.ir_mode == "SmartIR"


def test_cam_settings_negative_01():
    json_dict = load_json("sample_atl_canonical_camera_configuration.json")
    json_dict["encoding"]["quality"] = 7
    validation_error = None
    try:
        CameraConfiguration(**json_dict)
    except ValueError as e:
        validation_error = e
    assert validation_error is not None
