

import os
import json

from atollogy.data.schemas.ann_integ.ann_task_record import AnnotationTaskMediaRecord

FILE_BASE_DIR = "./sample_data/atollogy/data/schemas/ann_integ"


def load_json(filename):
    with open(os.path.join(FILE_BASE_DIR, filename)) as f:
        return json.load(f)


def test_ann_task_record():
    json_dict = load_json("ann_task_sample01.json")
    record = AnnotationTaskMediaRecord(**json_dict)
    assert 2 == len(record.media_file_dict)
    assert (str("s3://atl-prd-trimac-rawdata/"
                "image_readings/db8cam04_trimac_prd/video0/atlftpd/"
                "2019/11/06/05/"
                "38af29016a3b_video0_1573017258_atlftpd_20191106051418-snapshot.jpg") ==
            record.media_file_dict[
                "38af29016a3b_video0_1573017258_atlftpd_20191106051418-snapshot.jpg"
            ].orig_s3_url)
