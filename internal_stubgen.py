
import os
import subprocess

with open("dependency-stub-packages.txt", mode="r") as input_file:
    content = input_file.readlines()
    package_list = []
    line_no = 0
    for line in content:
        line_no = line_no + 1
        line = line.strip()
        if not line.startswith("#"):
            segs = line.split()
            segs_len = len(segs)
            if segs_len > 1:
                if not segs[1] == "#":
                    raise ValueError(
                        "unexpected usage of whitespace or '#' at line number: {}"
                            .format(line_no))
            if segs_len >=1:
                package_list.append(segs[0])
    print(package_list)
    os.makedirs(".stubgen/out",exist_ok=True)
    os.chdir(".stubgen")
    for package in package_list:
        print("About to run stubgen for package '{}' ... ".format(package))
        subprocess.run(["stubgen","-p",package],check=True)

