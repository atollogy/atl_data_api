
# This is a temporary script to install internal dependency from git
# within Bitbucket Pipeline env.
# Ordinary developer can just run a pip command directly, a
# after logging in with his / her Bitbucket account at a terminal :
# > pip install -r internal-requirements.txt
#
# This script will read "internal-requirements.txt"
# and inject the given git USER_NAME:PASSWORD into the git url


import sys
import subprocess
import uuid
import os


def main():
    if len(sys.argv)!=2:
        print("Expecting a git user_name:password being passed in")
        exit(1)
    out_file_name = generate_temp_file(sys.argv[1])
    print("out_file_name={}".format(out_file_name))
    print("=========================")
    subprocess.run(["pip", "install", "--ignore-installed", "-r", out_file_name])
    print("=========================")
    os.remove(out_file_name)

def generate_temp_file(git_user_name_password: str) -> str:

    out_file_name = "/tmp/tmp-{}-internal-requirements.txt".format(uuid.uuid4())
    new_git_url_start = "git+https://{}@bitbucket.org/".format(git_user_name_password)
    with open("./internal-requirements.txt",mode="r") as input_file:
        with open(out_file_name,mode="w+") as output_file:
            content = input_file.readlines()
            for line in content:
                if line.startswith("#"):
                    output_file.write(line)
                else:
                    output_file.write(line.replace("git+https://bitbucket.org/",new_git_url_start))
    return out_file_name

if __name__ == "__main__":
    main()
