#!/usr/bin/env python3.6

from setuptools import setup  # pyre-ignore[21]
import subprocess as sp
# Undefined import [21]: Could not find a module corresponding to import

data_dir = []
for d in sp.check_output(['find', 'sample_data/atollogy', '-type', 'd']).decode().split('\n')[:-1]:
    data_dir.append((f'share/doc/atollogy/{d}', sp.check_output(['find', d, '-type', 'f']).decode().split('\n')[:-1]))

setup(
    name='atl_data_api',
    author="Atollogy Inc.",
    author_email="eng@atollogy.com",
    classifiers=[
        "Development Status :: 5 - Production/Stable ",
        "Intended Audience :: Internal",
        "Topic :: Core Platform :: JSON SCHEMA",
        "License :: Commercial :: Atollogy",
        "Programming Language :: Python :: 3.6",
    ],
    data_files=data_dir,
    description='A module that demonstrates internal inter-repo dependencies',
    include_package_data=True,
    install_requires=[
        "pydantic==0.20",
        "isodate",
        "pytz"
    ],
    license="Commercial License - All rights reserved, Atollogy Inc.",
    packages=[path.replace('/', '.') for path in
              sp.check_output(['find', 'atollogy', '-type', 'd']).decode().split('\n')[:-1]
              if '__' not in path],
    package_dir={'atollogy': 'atollogy',
                 'data': 'atollogy/data',
                 'schemas': 'atollogy/data/schemas',
                 'inheritance_merge': 'atollogy/data/inheritance_merge',
                 },
    url="https://bitbucket.org/atollogy/atl_data_api",
    version='1.0.5',
    zip_safe=False
)
