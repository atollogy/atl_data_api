
# not using "pyre"-"strict"

import copy
from typing import List, Dict, Type, Any, Optional, Tuple

from atollogy.data.inheritance_merge.mergeable_types import MergeExtendList


def inheritance_merge(
        input_dict_list: List[Dict],
        data_class: Optional[Type],
        pre_merge_validate: bool = False,
        post_merge_validate: bool = True) -> Tuple[Dict, Any]:
    # Missing return annotation [3]:
    # Return type must be specified as type that does not contain `Any`.
    #
    # Missing parameter annotation [2]: Parameter `...`
    # must have a type that does not contain `Any`.

    if not input_dict_list:
        raise ValueError("'input_dict_list' is not defined.")
    if pre_merge_validate and data_class:
        for input_dict in input_dict_list:
            validate_dict(input_dict, data_class)

    merged_dict: Dict[str, Any] = {}
    post_merge_obj = None
    for input_dict in input_dict_list:
        new_merged_dict = _merge_dict(
            merged_dict, input_dict, data_class=data_class)
        merged_dict = new_merged_dict

    if post_merge_validate and data_class:
        post_merge_obj = validate_dict(merged_dict, data_class)
    return merged_dict, post_merge_obj


def intersect(list_a: List, list_b: List) -> List:
    """ return the intersection of two lists """
    return list(set(list_a) & set(list_b))


def union(list_a: List, list_b: List) -> List:
    """ return the union of two lists """
    return list(set(list_a) | set(list_b))


def is_simple_type(val):
    if val is None:
        return True
    if isinstance(val, (int, float, str, bool)):
        return True
    if isinstance(val, (list, dict)):
        return False
    return False


def _field_is_merge_exend_list(field_type_info: type) -> bool:
    # Missing parameter annotation [2]: Parameter `field_type_info`
    # must have a type that does not contain `Any`.
    try:
        if str(type(field_type_info)) == "typing.Union":    # pylint: disable=no-else-return
            # field_type_info=typing.Union[typing.List[str], NoneType]
            # type(field_type_info)=typing.Union
            for union_type_arg in field_type_info.__args__:
                if _field_is_merge_exend_list(union_type_arg):
                    return True
            return False
        else:
            origin = field_type_info.__dict__.get("__origin__")
            return origin == MergeExtendList if origin else False
    except AttributeError:
        # e.g. AttributeError: 'xxx' object has no attribute '__dict__'
        return False


def _merge_list(list1: List, list2: List, field_type: Optional[Type]) -> List:
    if _field_is_merge_exend_list(field_type):  # pylint: disable=no-else-return
        new_list = []
        new_list.extend(list1)
        new_list.extend(list2)
        # TODO doing deep copy for each value under val1 and val2
        return new_list
    else:
        return copy.deepcopy(list2)


def _merge_not_none_field(val1: Any, val2: Any, field_type: Optional[Type]) -> Any:
    if isinstance(val1, list):
        if isinstance(val1, list):
            val = _merge_list(val1, val2, field_type)
        else:
            raise ValueError(
                "Incompatible type values cannot be merged: {}, {}".format(
                    type(val1), type(val2)))
    elif isinstance(val1, dict):
        if isinstance(val2, dict):
            val = _merge_dict(val1, val2, field_type)
        else:
            raise ValueError(
                "Incompatible type values cannot be merged: {}, {}".format(
                    type(val1), type(val2)))
    else:
        val = copy.deepcopy(val2)
    return val


def _get_field_type_annotation(
        data_class: Optional[Type], field_name: str) -> Optional[Type]:
    if data_class:
        try:
            return data_class.__annotations__.get(field_name)
        except AttributeError:
            # AttributeError: type object 'Dict' has no attribute '__annotations__'
            return None
    else:
        return None


def _merge_dict(dict1: Dict[str, Any], dict2: Dict[str, Any],
                data_class: Optional[Type]) -> Optional[Dict]:
    if dict1 is None:    # pylint: disable=no-else-return
        return None if dict2 is None else copy.deepcopy(dict2)
    else:
        if dict2 is None:
            return copy.deepcopy(dict1)

    # after the above if-then-else, both dict1 and dict2 are None

    union_keys: List[str] = union(dict1.keys(), dict2.keys())   # pyre-ignore[6]
    # Incompatible parameter type [6]:
    # Expected `List[typing.Any]` for 1st anonymous parameter to
    # call `union` but got `typing.KeysView[str]`.

    result_dict = {}
    for key in union_keys:
        val1 = dict1.get(key)
        val2 = dict2.get(key)
        field_type = _get_field_type_annotation(data_class, key)
        if val1 is not None:
            if val2 is not None:
                result_dict[key] = _merge_not_none_field(val1, val2, field_type)
            else:
                result_dict[key] = copy.deepcopy(val1)
        else:
            if val2 is not None:
                result_dict[key] = copy.deepcopy(val2)
            else:
                result_dict[key] = None
    return result_dict


def validate_dict(input_dict: Dict, data_class: Type) -> Any:
    # Missing return annotation [3]: Return type
    # must be specified as type other than `Any`.
    # Missing parameter annotation [2]: Parameter `*`
    # must have a type that does not contain `Any`.
    return data_class(**input_dict)
