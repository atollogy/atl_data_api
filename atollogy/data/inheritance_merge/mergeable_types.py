from typing import List, MutableSequence, TypeVar

#  Any type.
T = TypeVar('T')        #pylint: disable=invalid-name
# Class name "T" doesn't conform to PascalCase naming style (invalid-name)


class MergeExtendList(List, MutableSequence[T], extra=list):
    pass
