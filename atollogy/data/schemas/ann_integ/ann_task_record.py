
# pyre-strict
from typing import Dict, Optional

from pydantic import BaseModel

from atollogy.data.schemas.mv_results.mv_inference_reqs import TFBbox


class ImageSize(BaseModel):
    width: int      # pyre-ignore[13]
    height: int     # pyre-ignore[13]


class AnnotationMediaRecord(BaseModel):
    orig_s3_url: str    # pyre-ignore[13]
    image_step_id: Optional[int]
    resolution: Optional[ImageSize]
    crop: Optional[TFBbox]


class AnnotationTaskMediaRecord(BaseModel):
    task_base_s3_folder: str        # pyre-ignore[13]
    """
    For example:
    "s3://atl-prd-annotation-cloudfactory/source_media/tasks/vh_class_task_20191112/"
    """
    media_file_dict: Dict[str, AnnotationMediaRecord] = {}
    """
    An example of the dictionary key:
    "14a78b1d2cdd_video0_1573058249_atlftpd_20191106163729-capture.jpg"
    """
