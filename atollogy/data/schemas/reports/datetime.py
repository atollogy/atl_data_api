
# pyre-strict

from enum import Enum
import datetime
from typing import Optional

from atollogy.data.schemas.validatable_model import PostValidatableBaseModel

# ---------------------------------
# TODO: We would consider moving from using "pydantic.BaseModel" as a base class
# to using decorator "@pydantic.dataclasses.dataclass",
# after more support is added under that decorator
# ---------------------------------
#
# See also:
# https://github.com/samuelcolvin/pydantic/issues/302
# https://github.com/samuelcolvin/pydantic/pull/285
#
# Then, we may not need to add ":type: SomeFooType" docstring anymore
#

# Uninitialized attribute [13]: Attribute `...` is declared in class `...`
# to have non-optional type `...` but is never initialized.


class LocalTimezoneEnum(str, Enum):
    """
        An enum that represents which local time zone is being used.
        E.g. "user" vs "subject" (e.g. a mill or an airport)
    """

    user: str = 'user'
    """
        using user's local time zone
    """

    subject: str = 'subject'
    """
        using the local time zone of a subject
        (e.g. the local timezone of a mill or an airport)
    """


class DateTimeIso8601(PostValidatableBaseModel):
    """
        A data class that combines an ISO-8601 date time field with a local timezone indicator
    """

    iso8601_datetime: datetime.datetime     # pyre-ignore[13]
    """
        :type: datetime.datetime
    """

    local_timezone: Optional[LocalTimezoneEnum] = None
    """
        :type: Optional[LocalTimezoneEnum]

        This 'DateTimeIso8601' data item should have 'local_timezone' (e.g. 'user' vs 'subject'),
        if and only if iso8601_datetime does not have a timezone designator
    """

    def post_conversation_validate(self) -> None:
        self.validate_local_timezone()

    def validate_local_timezone(self) -> None:
        if not self.iso8601_datetime:
            raise ValueError(f"'iso8601_datetime' is missing")

        if self.local_timezone:
            if self.iso8601_datetime.utcoffset() is not None:
                raise ValueError(
                    f"'local_timezone' value is extraneous, "
                    f"when datetime is already associated with an offset")
        else:
            if self.iso8601_datetime.utcoffset() is None:
                raise ValueError(
                    f"'local_timezone' value is missing for datetime without offset")
        # This 'DateTimeIso8601' data item should have 'local_timezone' (e.g. 'user' vs 'subject'),
        # if and only if iso8601_datetime does not have a timezone designator


class DateTimeIso8601Result(DateTimeIso8601):
    """
        This extended class is used to denote the conversion result in Reports
        from "iso8601_datetime" to an Epoch Second.

        This field is particularly useful when "local_timezone" is used,
        as the caller / retriever of an Atollogy Report may not know the local timezone
        of a machine or a subject (e.g. mill / airport)
    """

    epoch_second: int       # pyre-ignore[13]
    """
        :type: int

        An example of Epoch Second conversion is:
        "2018-11-01T00:00:00Z" (GMT/UTC) => 1541030400
    """

    def post_conversation_validate(self) -> None:
        super().post_conversation_validate()
        self.validate_epoch()

    def validate_epoch(self) -> None:
        if self.iso8601_datetime is None:
            raise ValueError('"iso8601_datetime" is missing')

        if self.iso8601_datetime.utcoffset() is not None:
            if int(self.iso8601_datetime.timestamp()) != int(self.epoch_second):
                raise ValueError(
                    'values of "iso8601_datetime" and "epoch_second" are inconsistent')
