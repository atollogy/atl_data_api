
# pyre-strict

from enum import Enum
import datetime
from typing import Dict, Optional, List, Union

from pydantic import BaseModel, validator

from atollogy.data.schemas.validatable_model import PostValidatableBaseModel
from atollogy.data.schemas.reports.datetime import DateTimeIso8601Result
from atollogy.data.schemas.reports.report_req_result import ReportResultBase

# There are two styles of data sharing:
# * "Report Document" style that is more fit for WebUI reporting
# * "Event" style that is more fit for log-event / SQL  query
# Some of data schema components can be reused between these two styles


class StatUnitKeyEnum(str, Enum):
    """
        Enum that represents unit of measure associated with a statistic
    """
    duration_minute: str = "duration_minute"
    """
    """
    duration_hour: str = "duration_hour"
    """
    """
    duration_day: str = "duration_day"
    """
    """
    count_cycle: str = "count_cycle"
    """
    """
    # more to add here (?)


class StatComparison(BaseModel):
    """
        Represents the comparison of a statistic against a previous period
    """

    prev_period_iso8601_duration: datetime.timedelta    # pyre-ignore[13]
    """
        :type: datetime.timedelta

        E.g. "P1D" = previous 1 day, where the first "P" means "Period"
    """
    change_percentage: float    # pyre-ignore[13]
    """
        :type: float
    """


class AggregateStatValue(BaseModel):
    """
        Represents a particular aggregrate statistic value along
        with an optional comparison against a previous period
    """

    value: float    # pyre-ignore[13]
    """
        :type: float
    """
    unit_key: StatUnitKeyEnum   # pyre-ignore[13]
    """
        :type: StatUnitKeyEnum
    """
    stat_comparison: Optional[StatComparison]
    """
        :type: Optional[StatComparison]
    """


class MediaInfo(BaseModel):
    """
        Represents information of a Media (used within "media_info_bundle")
    """

    link_url: str   # pyre-ignore[13]
    """
        :type: str

        For example:
        "/portal/image/customer_foo/ffffffffff27/video0/1518588840/stand_27/annotated.jpg"
        When the Link URL starts with "/". it means the URL is server-relative.
        E.g. relative to server "https://atollogy.com"
    """
    content_type: str   # pyre-ignore[13]
    """
        :type: str

        content type of the media (e.g. "image/jpeg")
    """
    start_time: Optional[DateTimeIso8601Result]
    """
        :type: Optional[DateTimeIso8601Result]

        The optional Start Time indicator of a media.
        The start time of a particular media can be different
        from the timestamp the over all Media Info Bundle
        (stored at "TimeLineMediaLink" structure),
        particularly for the case of video.
    """
    end_time: Optional[DateTimeIso8601Result]
    """
        :type: Optional[DateTimeIso8601Result]

        The optional End Time indicator of a media.
        This optional End Time indicator is mainly applicable to video media only.
    """
    media_src_id: Optional[str]
    """
        :type: Optional[str]

        An optional field to indicate the source ID of a media. (e.g. a camera ID)
    """


class TimeLineMediaLink(BaseModel):
    """
        Represents media (e.g. images or videos) that supports determination of status
        of a subject (e.g. mill or airport gateway) at a particular point of time.
    """

    media_info_bundle: List[MediaInfo]  # pyre-ignore[13]
    """
        :type: List[MediaInfo]

        Represents a set of Media Information Bundle.
        At a particular moment of time, multiple media can be used to support
        a status determination decision.  E.g. images from multiple camera,
        images + videos.
    """
    start_time: DateTimeIso8601Result   # pyre-ignore[13]
    """
        :type: DateTimeIso8601Result

        Represents the starting time of this Media Info Bundle
    """


class TimeLinePeriod(PostValidatableBaseModel):
    """
        Represents a period that is associated with a particularly status within a time line
    """

    start_time: DateTimeIso8601Result   # pyre-ignore[13]
    """
        :type: DateTimeIso8601Result

        start time of a "TimeLinePeriod" can be earlier than the start of a shift
    """
    end_time: Optional[DateTimeIso8601Result]
    """
        :type: Optional[DateTimeIso8601Result]

        end time of a "TimeLinePeriod" can be later than the end of a shift
        end time is Optional, as it may be a period that has not ended yet.
    """
    status: str     # pyre-ignore[13]
    """
        :type: str

        This "status" str could be subject-specific and timeline-specific.
        For example: for ground operation at an airport, for "composite" timeline,
        it would be: "arriving", "departing", "servicing", "idle"
    """
    data_unavailable: Optional[bool] = False
    """
        :type: Optional[bool]

        An example of data unavailable is:
        camera or other sensors are offline due to power outage.
        In a sense, "data_unavailable" is a special case of "status"
    """
    media_links: Optional[List[TimeLineMediaLink]]
    """
        :type: Optional[List[TimeLineMediaLink]]

        A collection of media links at one or more point of time
    """

    def post_conversation_validate(self) -> None:
        self.validate_start_time_end_time()

    def validate_start_time_end_time(self) -> None:
        if self.end_time:
            if self.end_time.epoch_second < self.start_time.epoch_second:
                raise ValueError("end_time < start_time")


class TimelineIndividualStatusSummary(BaseModel):
    """
        Summary Statistic for an Individual Timeline (typically for a particular status)
    """

    average: AggregateStatValue     # pyre-ignore[13]
    """
        :type: AggregateStatValue

        average statistics
    """
    total: AggregateStatValue       # pyre-ignore[13]
    """
        :type: AggregateStatValue

        total accumulated statistics
    """
    total_percentage_in_timeline: float     # pyre-ignore[13]
    """
        :type: float

        total percentage of the corresponding status within this timeline
    """


class TimeLine(BaseModel):
    """
        Represents a Time Line for a particular measure
    """

    timeline_periods: List[TimeLinePeriod]      # pyre-ignore[13]
    """
        :type: List[TimeLinePeriod]

        the list of period of different status within this time line
    """
    timeline_status_summary: Optional[Dict[str, TimelineIndividualStatusSummary]]
    """
        :type: Optional[Dict[str, TimelineIndividualStatusSummary]]

        Under "timeline_status_summary", dictionary key is corresponding to "status"
        For example: for ground operation at airport, for "composite" timeline,
        it would be: "arriving", "departing", "servicing", "idle" and etc.
    """

    @validator('timeline_periods', whole=True)
    def check_timeline_periods(cls, timeline_periods: List[TimeLinePeriod]) -> List[TimeLinePeriod]:
        if timeline_periods:
            timeline_periods_len = len(timeline_periods)
            for i in range(0, timeline_periods_len):
                if i < timeline_periods_len - 1:
                    if timeline_periods[i].end_time:
                        if (timeline_periods[i].end_time.epoch_second >
                                timeline_periods[i+1].start_time.epoch_second):
                            raise ValueError(
                                ('timeline_periods -> {} -> end_time overlaps '
                                 'with start_time of next timeline_period; ').format(i))
                    else:
                        raise ValueError(
                            ('timeline_periods -> {} -> end_time is unspecified; '
                             'only the last end_time can be left unspecified').format(i))
        return timeline_periods

    # TODO: to add validator for "timeline_periods"
    # to make sure there are no overlap between timeline periods


class SubjectReport(ReportResultBase):
    """
        Represents a Subject Report
        (for a customer, a subject, and a shift / start-time)
    """

    aggregate_stats: Optional[Dict[str, AggregateStatValue]]
    """
        :type: Optional[Dict[str, AggregateStatValue]]

        aggregate_stat Dict Key is a Statistic Name. E.g. "Average Cycle Time"
    """
    timelines: Dict[str, TimeLine]      # pyre-ignore[13]
    """
        :type: Dict[str, TimeLine]

        For airport ground operation use case,
        Dict Key of "timelines" are: "composite", "aircraft", "belt loader"
    """


class StatusEventTypeEnum(str, Enum):
    """
        Enum that represents Status Event Type
    """
    status_detected_change_to_on: str = "status_detected_change_to_on"
    """
        Represents a particular status has been detected to change to be ON
    """
    status_detected_change_to_off: str = "status_detected_change_to_off"
    """
        Represents a particular status has been detected to change to be OFF
    """
    status_detected_maintained_on: str = "status_detected_maintained_on"
    """
        Optionally, events will be emitted even when a status is maintained the same (ON).
        One of these purposes of this even type to share related media (image or video) files,
        while a particular status is determined being maintained ON.
    """
    status_detected_maintained_off: str = "status_detected_maintained_off"
    """
        Optionally, events will be emitted even when a status is maintained the same (OFF).
        One of these purposes of this even type to share related media (image or video) files,
        while a particular status is determined being maintained OFF.
    """


class SubjectStatusEvent(ReportResultBase):
    """
        Represents a Subject Status Event.
        An example of such an event for Airport Ground Operation is:
        Atollogy detects airplane "arriving" status has become "on" for "composite" timeline.
    """
    event_time_line: str        # pyre-ignore[13]
    """
        :type: str

        Represents the name of Event Timeline.
        For Ground Operation of an Airport, examples would be:
        "composite", "aircraft", "belt loader" and etc
    """
    status: str                 # pyre-ignore[13]
    """
        :type: str

        Represents status associated with this event
        For example, for "composite" timeline for Airport Ground Operation,
        status value would be: "idle", "arriving", "servicing", "departing".
    """
    status_event_type: Optional[StatusEventTypeEnum]
    """
        :type: StatusEventTypeEnum

        Represents the type of status event.
        E.g. whether the event is the detection of the corresponding status has become ON.
    """
    media_info_bundle: Optional[List[MediaInfo]]
    """
        :type: Optional[List[MediaInfo]]

        Represents an optional media bundle that supports the determination
        of status of this event
    """



SimpleType = Union[str, float, bool]
"""
    Simple Data Type: e.g. String, Float, Boolean
"""


class WeightUnit(str, Enum):
    """
    Enum for Weight Unit
    """
    lbs: str = "lbs"
    """
    "lbs"
    """


class WeightProperties(BaseModel):
    """
        Properties related to Weight.
    """
    weight_num: float                # pyre-ignore[13]
    """
        :type: float
    """
    weight_unit: WeightUnit          # pyre-ignore[13]
    """
        :type: WeightUnit
    """


class AssetProperties(BaseModel):
    """
        This class holds properties of an asset.
        E.g. weight
    """
    weight: Optional[WeightProperties]
    """
        :type: Optional[WeightProperties]
    """
    other_props: Optional[Dict[str, SimpleType]]
    """
        :type: Optional[Dict[str, SimpleType]]
    """


class AssetStatusEvent(SubjectStatusEvent):
    """
        Represents an Asset Status Event.
        An example of such an event for Yard Operation is:
        Atollogy detects a Tractor which has License Plate "A12345"  "entering"
        at "Wash Bay 7" for "trip" timeline.
    """
    asset_properties: Optional[AssetProperties]
    """
        :type: Optional[AssetProperties]

        Represents an optional set of properties for identified asset
    """


'''
    Example of image URL: 
    https://atollogy.com/portal/image/gatwickairport/
    ffffffffff27/video0/1518588840/stand_27_gatwick/annotated.jpg
    => 
    https://atl-prd-gatwickairport-data.s3.us-west-2.amazonaws.com/
    ground_operations/sec27_gatwickairport_prd/video0/
    stand_27_gatwick/annotated/2018/02/14/06/
    ffffffffff27_video0_1518588840_stand_27_gatwick_image_annotated.jpg?
    AWSAccessKeyId=...&Expires=1543270164&Signature=...    
'''
