# pyre-strict

from typing import Optional

from pydantic import BaseModel

from atollogy.data.schemas.reports.datetime import DateTimeIso8601, DateTimeIso8601Result


class AssetId(BaseModel):
    """
    This class represents a set of fields that allow us to uniquely identify an asset
    (e.g. a tractor)
    """
    id: str           # pyre-ignore[13]
    """
        :type: str
    """
    asset_type: str         # pyre-ignore[13]
    """
        :type: str
    """
    id_type: Optional[str]
    """
        :type: Optional[str]
    """

class ReportRequestBase(BaseModel):
    customer_name: str      # pyre-ignore[13]
    facility: Optional[str]
    subject_name: Optional[str]
    asset_id: Optional[AssetId]
    start_date_time: DateTimeIso8601        # pyre-ignore[13]


class ReportResultBase(BaseModel):
    """
        Base class of Report Result
    """

    customer_name: str  # pyre-ignore[13]
    """
        :type: str
    """
    facility: Optional[str]
    """
        :type: Optional[str]
    """
    subject_name: Optional[str]
    """
        :type: Optional[str]
    """
    asset_id: Optional[AssetId]
    """
        :type: Optional[AssetId]
    """

    shift: Optional[str]
    """
        :type: Optional[str]
    """
    start_date_time: DateTimeIso8601Result  # pyre-ignore[13]
    """
        :type: DateTimeIso8601Result
    """
    # TODO: decide whether we need "end_date_time" here
