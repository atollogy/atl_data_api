from pydantic import BaseModel


class PostValidatableBaseModel(BaseModel):

    def __init__(self, **data) -> None:
        super().__init__(**data)
        self.post_conversation_validate()

    def post_conversation_validate(self) -> None:
        pass
