
# pyre-strict
from typing import Optional, Dict, Any

from pydantic import BaseModel

# See: https://atollogy.atlassian.net/
# wiki/spaces/CV/pages/693731671/LPR+QC+Engineering+Execution+2019-09


class FuzzyMatchedLpnQCProps(BaseModel):
    override_result: bool       # pyre-ignore[13]
    original_result: str        # pyre-ignore[13]
    candidate_lpn: Optional[str]
    fuzzy_result: Optional[str]
    episodic_result: Optional[str]
    episodic_run_epoch: Optional[float]


class ImgQcProps(BaseModel):
    fuzzy_matched_lpn: Optional[FuzzyMatchedLpnQCProps]
    # skew_angle_sideread: Optional[bool]

'''
        "fuzzy_matched_lpn": {
            "override_result": false,
            "original_result": "A12345",
            "candidate_lpn": "A12345",
            "fuzzy_result": "A1234S"
        },
        "skew_angle_sideread": true
'''


class LpnAccuracy(BaseModel):
    original_result: Optional[bool]
    fuzzy_result: Optional[bool]
    corrected_result: Optional[str]
    not_sure: Optional[bool]


class LpnQCResult(BaseModel):
    accuracy: LpnAccuracy           # pyre-ignore[13]
    image_attributes: Optional[Dict[str, Any]] = {}
    side_read: Optional[bool]
    excluded_for_accuracy: Optional[bool]

'''
{
    "lpn_accuracy": {
        "original_result": false,
        "fuzzy_result": false,
        "corrected_result": "A12347"
        "not_sure": false
    },
    "image_attributes": {
        "dusty_on_len": true,
        "dirty_plate": true,
        "other": "insect flying"
    }
}
'''
