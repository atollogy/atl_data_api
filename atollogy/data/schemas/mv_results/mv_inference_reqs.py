
# pyre-strict

from typing import Optional, Dict, Any, List

from pydantic import BaseModel

from atollogy.data.schemas.reports.datetime import DateTimeIso8601Result

INFERENCE_OP_PERSON_DETECTION: str = "person_detection"
INFERENCE_OP_TINY_PERSON_DETECTION: str = "tiny_person_detection"
INFERENCE_OP_TRACTOR_TRAILER: str = "tractor_trailer"


class TFBbox(BaseModel):
    xmin: int       # pyre-ignore[13]
    xmax: int       # pyre-ignore[13]
    ymin: int       # pyre-ignore[13]
    ymax: int       # pyre-ignore[13]


class MvInferenceRequestBase(BaseModel):
    req_uuid: str           # pyre-ignore[13]
    """
        Using a Request UUID to provide easy correlation
        between Inference Request and Result.

        This field is different from "event_uuid" for input event (e.g. raw event)
        This "req_uuid" field can be reused as the "event_uuid" of output event
        (e.g. cv processed event)
    """

    tenant: str        # pyre-ignore[13]
    """
        In short term, we would still overload "tenant"
        with both customer and site concept. e.g. "cemexclayton"
        In long term, we would split this concept into 2 distinct fields:
        "tenant" and "facility".
        E.g.:
        {
           "tenant": "cemex",
           "facility": "clayton"
        }
    """
    facility: Optional[str]

    camera_mac_addr: str    # pyre-ignore[13]
    camera_node_name: Optional[str]
    collection_time: DateTimeIso8601Result             # pyre-ignore[13]

    s3_url: str             # pyre-ignore[13]
    inference_op: str       # pyre-ignore[13]

    misc_inference_parameters: Optional[Dict[str, Any]]
    """
       Example of Misc Inference Parameter are:
       * filtering result parameter
       * cropping parameter
       * preference of mix of models or human and etc
    """

    input_crop_list: Optional[List[TFBbox]]
    """
        This 'input_crop_list' is particularly useful for Image Classification.
        So that Image Classification can run its Inferences on multiple focus crop area.

        This 'input_crop_list' can be useful for Object Detection as well.
        This will reduce the input size to the Object Detection Inference operation,
        for better operation efficiency.
        For Object Detection, it would accept an "input_crop_list" of zero or one box.
    """
