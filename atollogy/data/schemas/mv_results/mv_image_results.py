
# pyre-strict
from enum import Enum
from typing import Optional, List, Set, Dict, Any

from pydantic import BaseModel

from atollogy.data.schemas.mv_results.mv_inference_reqs import TFBbox
from atollogy.data.schemas.reports.datetime import DateTimeIso8601

# For TR Record used during Internal Training, see also:
# https://atollogy.atlassian.net/wiki/spaces/CV/pages/480378926/TF+Record+Format

# TODO: work with Aarti, Duck-Ha and Zac to resolve inconsistent usage of fields:
#  "id", "source_id", "filename"
from atollogy.data.schemas.validatable_model import PostValidatableBaseModel


class ResultSourceTypeEnum(str, Enum):
    human_result: str = "human_result"
    model_result: str = "model_result"
    # in future, we might support "ensemble_result" situation.


class MvImageResultBase(BaseModel):
    req_uuid: Optional[str]
    """
        Using this "req_uuid" to provide easy correlation

        between inference request and result.
        This field should be filled, when the request comes
        from a Request Message (class MvInferenceRequestBase),
        which has a "req_uuid".

        This field should be skipped, when result are provided
        via Human Annotation Tool, without a corresponding Request Message.
    """

    s3_url: str     # pyre-ignore[13]
    # Uninitialized attribute [13]: Attribute `s3_url` is declared in class `...`
    # to have non-optional type `str` but is never initialized.
    encoded_bytes: Optional[bytes] = None
    height: int     # pyre-ignore[13]
    width: int      # pyre-ignore[13]
    color_space: str = "RGB"
    channels: int = 3
    mime_type: str = "image/jpeg"

    # Note: the following 4 fields are added relatively recently
    result_gen_time: DateTimeIso8601                # pyre-ignore[13]
    result_source_type: ResultSourceTypeEnum        # pyre-ignore[13]
    human_user_id: Optional[str]
    model_s3_url: Optional[str]


class DetectedClass(BaseModel):
    text: str       # pyre-ignore[13]


class DetectedObject(BaseModel):
    bbox: TFBbox         # pyre-ignore[13]
    confidence_score: Optional[float]
    occluded: Optional[bool]
    partial_outside_frame: Optional[float]
    attrs: Optional[Dict[str, Any]]


class DetectedObjectGroup(BaseModel):
    detected_object_class: DetectedClass       # pyre-ignore[13]
    detected_object_list: List[DetectedObject]      # pyre-ignore[13]


class ObjectDetectionResult(MvImageResultBase):
    object_group_list: List[DetectedObjectGroup]   # pyre-ignore[13]


class ObjectQCResultTypeEnum(str, Enum):
    correct: str = "correct"
    not_sure: str = "not_sure"
    false_positive: str = "false_positive"
    false_negative: str = "false_negative"
    chg_bbox: str = "chg_bbox"
    chg_class: str = "chg_class"
    chg_bbox_class: str = "chg_bbox_class"


class DetectedObjectQCEntry(PostValidatableBaseModel):
    object_qc_type: ObjectQCResultTypeEnum      # pyre-ignore[13]
    target_class_text: Optional[str]
    target_obj_idx: Optional[int]
    new_class: Optional[str] = None
    new_bbox: Optional[TFBbox] = None

    def post_conversation_validate(self) -> None:
        if self.object_qc_type != ObjectQCResultTypeEnum.false_negative:
            if self.target_obj_idx is None or self.target_class_text is None:
                raise ValueError(str(
                    "object_qc_type cases, other than 'false_negative' case "
                    "should have target_class_text and target_obj_idx defined"))

        if self.object_qc_type in [
                ObjectQCResultTypeEnum.correct,
                ObjectQCResultTypeEnum.not_sure,
                ObjectQCResultTypeEnum.false_positive,
        ]:
            if self.new_class is not None or self.new_bbox is not None:
                raise ValueError(str(
                    "correct, not_sure and false_positive cases "
                    "do not need new_class or new_bbox"))
        elif self.object_qc_type in [
                ObjectQCResultTypeEnum.false_negative,
                ObjectQCResultTypeEnum.chg_bbox_class,
        ]:
            if self.new_class is None or self.new_bbox is None:
                raise ValueError(str(
                    "false_negative and chg_bbox_class cases "
                    "require both new_class and new_bbox"))
        elif self.object_qc_type == ObjectQCResultTypeEnum.chg_bbox:
            if self.new_class is not None or self.new_bbox is None:
                raise ValueError("chg_bbox case requires new_bbox, but not new_class")
        elif self.object_qc_type == ObjectQCResultTypeEnum.chg_class:
            if self.new_class is None or self.new_bbox is not None:
                raise ValueError("chg_class case requires new_class, but not new_bbox")


def validate_uniqueness_qc_obj_detection(
        qc_object_list: List[DetectedObjectQCEntry]
) -> None:
    unique_check_dict: Dict[str, Set[int]] = {}
    for entry in qc_object_list:
        if entry.target_class_text is not None:
            unique_check_set = unique_check_dict.get(entry.target_class_text)
            if unique_check_set is None:
                unique_check_set = set()
                unique_check_dict[entry.target_class_text] = unique_check_set

            if entry.target_obj_idx in unique_check_set:
                raise ValueError("duplicated entry for '{}' - {}".format(
                    entry.target_class_text, entry.target_obj_idx))

            if entry.target_obj_idx is not None:
                unique_check_set.add(entry.target_obj_idx)


class QCObjectDetectionResult(PostValidatableBaseModel):
    qc_object_list: List[DetectedObjectQCEntry]     # pyre-ignore[13]

    def post_conversation_validate(self) -> None:
        validate_uniqueness_qc_obj_detection(self.qc_object_list)


class ImageClassificationEntry(BaseModel):
    crop_bbox: Optional[TFBbox]
    detected_image_class: DetectedClass       # pyre-ignore[13]
    confidence_score: Optional[float]


class ImageClassificationResult(MvImageResultBase):
    classification_result_entries: List[ImageClassificationEntry]       # pyre-ignore[13]


class LpnIdInferred(str, Enum):
    false = "false"
    true = "true"


class IdBoxAnnotationInfo(BaseModel):
    bbox: TFBbox         # pyre-ignore[13]
    id_str: Optional[str]
    inferred: Optional[LpnIdInferred] = LpnIdInferred.false


class VehicleIDAnnotationInfo(BaseModel):
    lpn_info: Optional[IdBoxAnnotationInfo]


class VehicleClassificationIDResult(ImageClassificationResult):
    vehicle_id_annotation_list: Optional[List[VehicleIDAnnotationInfo]]
