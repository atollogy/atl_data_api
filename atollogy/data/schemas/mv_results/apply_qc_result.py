

# pyre-strict
import copy
from typing import List, Dict, Optional, Tuple

from atollogy.data.schemas.mv_results.mv_image_results \
    import DetectedObjectQCEntry, DetectedObjectGroup, \
    ObjectQCResultTypeEnum, DetectedObject


def _locate_qc_target_entry(
        qc_entry: DetectedObjectQCEntry,
        obj_group_dict: Dict[str, DetectedObjectGroup],
) -> Optional[DetectedObject]:
    if qc_entry.target_obj_idx is None:
        raise ValueError("target_obj_idx is None")
    if qc_entry.target_class_text is None:
        raise ValueError("target_class_text is None")

    grp = obj_group_dict.get(qc_entry.target_class_text)
    if grp and grp.detected_object_list:
        if qc_entry.target_obj_idx < len(grp.detected_object_list):
            return grp.detected_object_list[qc_entry.target_obj_idx]
    return None



def apply_detected_obj_qc_result(       # pylint: disable=too-many-branches
        qc_object_list: List[DetectedObjectQCEntry],
        orig_object_group_list: List[DetectedObjectGroup],
) -> List[DetectedObjectGroup]:

    object_group_list = copy.deepcopy(orig_object_group_list)

    obj_group_dict: Dict[str, DetectedObjectGroup] = {}
    for grp in object_group_list:
        obj_group_dict[grp.detected_object_class.text] = grp

    to_remove_list: List[Tuple[DetectedObjectQCEntry, DetectedObject]] = []

    for qc_entry in qc_object_list:

        if qc_entry.object_qc_type in [
                ObjectQCResultTypeEnum.correct,
                ObjectQCResultTypeEnum.not_sure,
        ]:
            # do nothing
            pass
        elif qc_entry.object_qc_type == ObjectQCResultTypeEnum.false_positive:
            to_remove_entry = _locate_qc_target_entry(qc_entry, obj_group_dict)
            if to_remove_entry:
                to_remove_list.append((qc_entry, to_remove_entry))
        elif qc_entry.object_qc_type == ObjectQCResultTypeEnum.false_negative:
            new_detected_obj = DetectedObject(bbox=qc_entry.new_bbox)
            if qc_entry.new_class is not None:
                obj_group_dict[qc_entry.new_class].detected_object_list.append(
                    new_detected_obj)
            else:
                # this block is unreachable because of other validation logic
                raise RuntimeError("new_class is missing under a 'false_negative' entry")
        elif qc_entry.object_qc_type == ObjectQCResultTypeEnum.chg_bbox:
            target_entry = _locate_qc_target_entry(qc_entry, obj_group_dict)
            target_entry.bbox = qc_entry.new_bbox
            target_entry.confidence_score = None
        elif qc_entry.object_qc_type in [
                ObjectQCResultTypeEnum.chg_bbox_class,
                ObjectQCResultTypeEnum.chg_class,
        ]:
            to_remove_entry = _locate_qc_target_entry(qc_entry, obj_group_dict)
            if to_remove_entry and qc_entry.new_class:
                to_remove_list.append((qc_entry, to_remove_entry))
                if qc_entry.new_bbox:
                    new_detected_obj = DetectedObject(bbox=qc_entry.new_bbox)
                else:
                    new_detected_obj = DetectedObject(bbox=to_remove_entry.bbox)
                obj_group_dict[qc_entry.new_class].detected_object_list.append(
                    new_detected_obj)


    for remove_entry in to_remove_list:
        obj_group_dict[
            remove_entry[0].target_class_text
        ].detected_object_list.remove(remove_entry[1])

    return list(obj_group_dict.values())
