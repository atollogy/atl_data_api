# pyre-strict

from enum import Enum
from typing import List, Dict, Optional
from pydantic import BaseModel, conint, constr

from atollogy.data.schemas.validatable_model import PostValidatableBaseModel

MAC_ADDRESS_REGEX: str = r'^[0-9A-Fa-f]{12}$'
IPV4_ADDRESS_REGEX: str = r'^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$'

class BackLightEnum(str, Enum):
    """
        Enum that represents BackLight modes
    """
    Off: str = "Off"

    HLC: str = "HLC"

    BLC: str = "BLC"

    WDR: str = "WDR"


class IRModeEnum(str, Enum):
    """
        Enum that represents IR modes
    """
    SmartIR: str = "SmartIR"

    ZoomPrio: str = "ZoomPrio"

    Off: str = "Off"

    Manual: str = "Manual"


class WhiteBalanceModesEnum(str, Enum):
    """
        Enum that represents WhiteBalance modes
    """

    auto: str = "Auto"

    outdoor: str = "Outdoor"

    street_lamp: str = "Street Lamp"

    natural: str = "Natural"

    regional_custom: str = "Regional Custom"


class ProtocolEnum(str, Enum):
    """
        Enum that represents WhiteBalance modes
    """

    FTP: str = "FTP"

    FTPS: str = "FTPS"

    # More to add here?

class ResolutionEnum(str, Enum):
    """
        Enum that represents Resolution modes
    """
    r_720: str = "1280x720"

    r_960: str = "1280x960"

    r_1080: str = "1920x1080"

    r_1296: str = "2304x1296"

    r_1520: str = "2688x1520"

    r_1944: str = "2592x1944"

    r_1728: str = "3072x1728"

    r_2048: str = "3072x2048"

    r_2160: str = "3840x2160"


class SnapshotSettings(BaseModel):
    """
        Component class 'SnapShotSettings' of CameraConfiguration
    """

    interval: int  # pyre-ignore[13]
    """
        :type: int
        interval of Snapshot capture (in seconds)
        Translates in to SnapFormat->Video->FPS in the db8 config
        The value for FPS would be 1 / interval
    """

    quality: Optional[conint(ge=1, le=6)]
    """
        :type: Optional[int]
        Default value is 6 for db8 cameras.
        Quality of Snapshots (range - 1 to 6 for db8 cams)
        Translates in to SnapFormat->Video->quality in the db8 config"
    """


class WhiteBalance(BaseModel):
    """
        Component class 'WhiteBalance' of CameraConfiguration
        Translates in to VideoInWhiteBalance->mode in the db8 config
    """

    mode: WhiteBalanceModesEnum  # pyre-ignore[13]
    """
        :type: str
        mode of WhiteBalance (ex. 'outdoor', 'auto', 'streetlamp' etc.
        Translates in to VideoInWhiteBalance->Mode in the db8 config
    """


class NetworkSettings(BaseModel):
    """
        Component class 'NetworkSettings' of CameraConfiguration
    """

    local_device_ip: constr(regex=IPV4_ADDRESS_REGEX)
    """
        :type: str
        Local IPv4 address of the camera device
        Translates in to IPAddress in the db8 config
    """

    default_gateway: constr(regex=IPV4_ADDRESS_REGEX)
    """
        :type: str
        Local IPv4 address of the default gateway
        Translates in to IPv6->DefaultGateway in the db8 config
    """


class VideoEncoding(BaseModel):
    """
        Component class 'VideoEncoding' of CameraConfiguration
        Translates to Video in db8 config
    """

    bit_rate: int  # pyre-ignore[13]
    """
        :type: int
        Bitrate of the video stream.
        Translates in to MainFormat->Video->BitRate in the db8 config
    """

    resolution: ResolutionEnum  # pyre-ignore[13]
    """
        :type: str
        Format widthxheight
        Ex. 1920x1080
        Translates in to SnapFormat[]->customResolutionName,
        and break the res in Height and Width in the db8 config
    """

    quality: Optional[conint(ge=1, le=6)]
    """
        :type: Optional[int]
        Quality of the snapshot
        Default value is 6 for db8 cameras.
        For db8 cams range is 1 to 6
        Translates in to MainFormat[]->Video->Quality,
        and SnapFormat[]->Video->Quality in the db8 config
    """

    white_balance: Optional[WhiteBalance]
    """
         :type: Optional[WhiteBalance]
         white_balance mode would be Auto by default
         Translates in to VideoInWhiteBalance in the db8 config
     """


class AutoReboot(BaseModel):
    """
        Component class 'AutoReboot' of CameraConfiguration
        Translates in to AutoMaintain in the db8 config
    """

    day_of_the_week: int  # pyre-ignore[13]
    """
        TBC: range: 1 - 7
        Translates in to AutoRebootDay in the db8 config
    """

    hour: int  # pyre-ignore[13]
    """
        TBC: range: 0 - 23
        Translates in to AutoRebootHour in the db8 config
    """

    minute: int  # pyre-ignore[13]
    """
        TBC: range: 0 - 59
        Translates in to AutoRebootMinute the db8 config
    """

    enable: bool  # pyre-ignore[13]
    """
        Enable or disable AutoReboot
    """


class FTP(PostValidatableBaseModel):
    """
        Component class 'FTP' of CameraConfiguration
        Translates in to NAS[0] in the db8 config
    """

    ip_address: constr(regex=IPV4_ADDRESS_REGEX)
    """
        :type: str
        IP address of the FTP server
        Translates in to NAS[0]->Address in the db8 config
    """

    user_name: str  # pyre-ignore[13]
    """
        :type: str
        Username of the FTP User - FTP Credential
        Translates in to NAS[0]->UserName in the db8 config
    """

    password: str  # pyre-ignore[13]
    """
        :type: str
        Password of the FTP User - FTP Credential
        Translates in to NAS[0]->Password in the db8 config
        future, not short term, TODO: password obfuscation
    """

    port: conint(ge=0, le=9999)
    """
        :type: int
        Port for connecting to the FTP server
        Translates in to NAS[0]->Port in the db8 config
    """

    relative_output_path: str  # pyre-ignore[13]
    """
        :type: str
        relative path to on the FTP server to store photos/videos from Camera
        Translates in to NAS[0]->Directory in the db8 config
        TODO: validation: cannot start with "/" and does not contain ".." * /
    """

    def post_conversation_validate(self) -> None:
        self.validate_relative_output_path()

    def validate_relative_output_path(self) -> None:
        if self.relative_output_path.startswith('/') or ".." in self.relative_output_path:
            raise ValueError("PATH cannot start with / AND cannot contain ..")

    protocol: ProtocolEnum  # pyre-ignore[13]
    """
        :type: ProtocolEnum
        Protocol for FTP connection. FTP/FTPS
        Translates in to NAS[0]->Protocol in the db8 config
    """

    timed_snapshot: bool  # pyre-ignore[13]
    """
        :type: bool
        Enable or disable storing of Timed Snapshot
        Translates in to RecordStoragePoint->TimingSnapShot in the db8 config
    """

    event_snapshot: bool  # pyre-ignore[13]
    """
        :type: bool
        Enable or disable storing of Event triggered Snapshot
        Translates in to RecordStoragePoint->VideoDetectSnapShot in the db8 config
    """

    timed_video: bool  # pyre-ignore[13]
    """
        :type: bool
        Enable or disable storing of Timed Video
        Translates in to RecordStoragePoint->TimingRecord in the db8 config
    """

    event_video: bool  # pyre-ignore[13]
    """
        :type: bool
        Enable or disable storing of Event triggered Video
        Translates in to RecordStoragePoint->VideoDetectRecord in the db8 config
    """


class MotionDetection(BaseModel):
    enable: bool  # pyre-ignore[13]
    """
        :type: bool
        Enable or Disable MotionDetection
        Translates in to MotionDetect->Enable in the db8 config
    """

    dahua_motion_detect_region: List[Dict]
    """
        :type: List[Dict]
        This is array of dictionaries.
        Which has a 4 dictionaries(4 regions,),
        representing a bitmap of activity trigger region
        db8 cam native Dictionary of Motion Detection.
        Translates in to MotionDetectWindow in the db8 config
    """

    sensitivity: conint(ge=0, le=100)
    """
        :type: int
        Percentage of the Pixels needs to be change in a particular cell,
        to mark the Cell active
        Translates in to MotionDetect->MotionDetectWindow->Sensitive in the db8 config
    """

    threshold: conint(ge=0, le=100)
    """
        :type: int
        No. of cells needs to be activated To consider activity - present
        Translates in to MotionDetect->MotionDetectWindow->threshold in the db8 config
    """

    min_event_duration_in_seconds: int  # pyre-ignore[13]
    """
        :type: int
        Event duration (Once triggered, how long the activity lasts)
        Translates in to MotionDetect->Dejitter in the db8 config
    """


class IRSettings(BaseModel):
    """
           Component class 'IRSettings' of CameraConfiguration
           Translates in to Lighting in the db8 config
    """
    ir_mode: IRModeEnum  # pyre-ignore[13]
    """
        :type: str
        IR Mode
    """

    far_light: Optional[conint(ge=0, le=100)]
    """
        :type: str
        Far Light - Should be in range (0, 100) inclusive
        Gets used if the mode is Manual
    """

    near_light: Optional[conint(ge=0, le=100)]
    """
        :type str
        Near Light - Should be in range (0, 100) inclusive
        Gets used if the mode is Manual
    """


class ColorSettings(BaseModel):

    brightness: conint(ge=0, le=100)
    """
        :type: int
        Brightness of the camera. Range (0, 100) inclusive
    """

    contrast: conint(ge=0, le=100)
    """
        :type: int
        contrast of the camera. Range (0, 100) inclusive
    """

    sharpness: conint(ge=0, le=100)
    """
        :type: int
        Sharpness of the camera. Range (0, 100) inclusive
    """

    saturation: conint(ge=0, le=100)
    """
        :type: int
        Saturation of the camera. Range (0, 100) inclusive
    """

    gamma: conint(ge=0, le=100)
    """
        :type: int
        Gamma of the camera. Range (0, 100) inclusive
    """


class BackLightSettings(BaseModel):

    backlight_mode: BackLightEnum  # pyre-ignore[13]
    """
        :type: Backlight mode
    """

    wdr_range: conint(ge=1, le=100)
    """
        :type: WDR range
    """

    hlc_range: conint(ge=1, le=100)
    """
        :type: HLC range
    """


class CameraConfiguration(BaseModel):
    """
        Base Atollogy Canonical CameraConfiguration class
    """

    device_id: constr(regex=MAC_ADDRESS_REGEX)
    """
        :type: str
        MAC address of the device
        Translates in to IPv6->Hostname, and General->MachineName in the db8 config
    """

    auto_sync_enabled: bool  # pyre-ignore[13]
    """
        :type: bool
        To Enable or Disable automatic Sync from repository to camera
    """

    default_config_list: Optional[List[str]]
    """
        :type: Optional[List[str]]
        Specifies the list of default to inherit from.
    """

    camera_channel_title: Optional[str]
    """
        :type: Optional[str]
        Camera channelTitle (gets displayed on the images)
        Would be None by default
        Translates in to ChannelTitle->Name in the db8 config
    """

    encoding: VideoEncoding  # pyre-ignore[13]
    """
        :type: VideoEncoding
        Camera encoding such as resolution, bitrate, resolution etc.
        Translates in to MainFormat->Video in the db8 config
    """

    network_settings: NetworkSettings  # pyre-ignore[13]
    """
        :type: NetworkSettings
    """

    timed_snapshot_settings: SnapshotSettings  # pyre-ignore[13]
    """
        :type: SnapshotSettings
        Settings for Timed Snapshots
    """

    event_snapshot_settings: SnapshotSettings  # pyre-ignore[13]
    """
        :type: SnapshotSettings
        Settings for motion event triggered Snapshots
    """

    motion_detection: MotionDetection  # pyre-ignore[13]
    """
        :type: MotionDetection
        This a dictionary of db8 motion_detection native JSON.
        Contains trigger region, sensitivity, zone parameters of motion_detection
    """

    ftp: FTP  # pyre-ignore[13]
    """
        :type: FTP
        To store snapshot/video on the FTP server.
    """

    auto_reboot: Optional[AutoReboot]
    """
        :type: Optional[AutoReboot]
        Auto Reboot would be OFF by default.
        Camera AutoReboot parameters.
    """

    ir_settings: Optional[IRSettings]
    """
        :type: Optional[IRSettings]
        ir_mode by default 'Auto'
        Represents which IR mode to use and ranges
    """

    color_settings: Optional[ColorSettings]
    """
        :type Optional[ColorSettings]
        Contains camera color settings such as brightmess, contrast, gamma etc.
    """

    backlight_settings: Optional[BackLightSettings]
    """
        :type Optional[BackLightSettings]
        Contains camera Backlight modes such as BLC, HLC etc. and ranges
    """
