
# pyre-strict

from pydantic import Extra

from atollogy.data.schemas.validatable_model import PostValidatableBaseModel


class BaseFuncCfg(PostValidatableBaseModel):
    function: str   # pyre-ignore[13]

    class Config:
        extra = Extra.allow        # pyre-ignore[4]
        # Missing attribute annotation [4]:
        # Attribute `...` of class `...` has no type specified.
        # Undefined attribute [16]:
        # Module `pydantic` has no attribute `main`.
