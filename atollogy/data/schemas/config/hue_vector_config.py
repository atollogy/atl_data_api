from typing import Dict, Tuple, Optional

from atollogy.data.schemas.config.base_func_config import BaseFuncCfg
from atollogy.data.schemas.validatable_model import PostValidatableBaseModel


class BWEnablement(PostValidatableBaseModel):
    white_saturation_threshold: Optional[float]
    white_hsv_value_threshold: Optional[float]
    black_saturation_threshold: Optional[float]
    black_hsv_value_threshold: Optional[float]


class HueSubjectDetails(PostValidatableBaseModel):
    zone: Tuple[int, int, int, int]
    outer_white_zone: Tuple[int, int, int, int]
    rgb_vector_labels: Dict[str, Tuple[int, int, int]]


'''
{
    "zone": [405, 530, 20, 20],
    "outer_white_zone": [399, 524, 32, 32],
    "black_white_enablement": {
        "white_saturation_threshold": 0.2,
        "white_hsv_value_threshold": 0.7
    },
    "rgb_vector_labels": {
        "red": [149, 95, 117],
        "yellow": [167, 146, 102],
        "green": [55, 94, 93],
        "blue": [70, 90, 158]
    }
}
'''


class HueVectorConfig(BaseFuncCfg):
    subjects: Dict[str, HueSubjectDetails]
