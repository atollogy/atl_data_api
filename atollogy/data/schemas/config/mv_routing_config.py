
# pyre-strict
import json
from typing import Dict, Type, Optional

from pydantic import Extra, ValidationError

from atollogy.data.inheritance_merge.mergeable_types import MergeExtendList
from atollogy.data.schemas.config.base_func_config import BaseFuncCfg
from atollogy.data.schemas.config.hue_vector_config import HueVectorConfig
from atollogy.data.schemas.validatable_model import PostValidatableBaseModel

# Uninitialized attribute [13]: Attribute `...` is declared in class `...`
# to have non-optional type `...` but is never initialized.

_FUNC_STEP_CFG_DICT: Dict[str, Type] = {
    "hue_vector_label": HueVectorConfig
}


class MVRouting(PostValidatableBaseModel):
    stepCfgs: Dict[str, BaseFuncCfg]        # pyre-ignore[13]
    steps: MergeExtendList[str]     # pyre-ignore[13]
    # similar to:
    #   steps: List[str]
    # just the custom type hint indicate how to handle merging

    class Config:
        extra = Extra.ignore        # pyre-ignore[4]
        # Missing attribute annotation [4]: Attribute `...` of class `...` has no type specified.

    _step_cfg_json_dict: Dict[str, Dict] = {}
    _step_cfg_specialized_obj_dict: Dict[str, BaseFuncCfg] = {}

    def post_conversation_validate(self) -> None:
        for step in self.steps:
            step_cfg = self.stepCfgs.get(step)
            if not step_cfg:
                raise ValueError(
                    "'stepCfgs' has this list of step key name: {}. ".format(
                        list(self.stepCfgs.keys())) +
                    "'steps' list entry: '{}' cannot be found".format(step))

        for step_name, step_cfg in self.stepCfgs.items():
            json_dict = json.loads(step_cfg.json())
            self._step_cfg_json_dict[step_name] = json_dict

            step_cfg_class = _FUNC_STEP_CFG_DICT.get(step_cfg.function)
            if step_cfg_class:
                try:
                    step_cfg_specialized_obj: BaseFuncCfg = step_cfg_class(**json_dict)
                    self._step_cfg_specialized_obj_dict[step_name] = step_cfg_specialized_obj
                except ValidationError as v_err:
                    raise ValueError("Step Function Name: {} :\n{}".format(
                        step_name, v_err)) from v_err

    def get_step_config_json_dict(          # pyre-ignore[3]
            self, step_name: str) -> Optional[Dict]:
        # Missing return annotation [3]:
        # Return type must be specified as type that does not contain `Any`.
        return self._step_cfg_json_dict.get(step_name)

    def get_step_config_obj(self, step_name: str) -> Optional[BaseFuncCfg]:
        return self._step_cfg_specialized_obj_dict.get(step_name)
